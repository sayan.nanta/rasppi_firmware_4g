from psutil import process_iter
import subprocess
import psutil
import os
import time
import logging
import datetime

# Logging
LOG_PATH = '/tmp/watchdog-'
LOG_INFO = '{0}info.log'.format(LOG_PATH)
LOG_SIZE = 512


log_file_name = 'log-info-{0}.log'.format(datetime.datetime.now().strftime("%Y-%m-%d"))
def log_init():
    logging.basicConfig(level=logging.INFO, filename=LOG_INFO,format='%(asctime)s: %(message)s')
    print('Init log..')


def info(message):
    logging.info(message)

log_init()
while True:
    procIsRun = False
    for proc in psutil.process_iter():
        try:
            # Get process name & pid from process object.
            processName = proc.name()
            processID = proc.pid
            processCmd = proc.cmdline()
    #         prnt(processName , ' ::: ', processID)
            if processName == 'python3':
    #             print(processName , ' ::: ', processID)
    #             print(processID, ' ::: ', proc.cmdline())
    #             print(processName)
    #             print(type(processName))
    #             print(processID)
    #             print(type(processID))
    #             print(processCmd)
    #             print(len(processCmd))
    #             co = 0
    #             for i in processCmd:
    #                 print(co,':',i)
    #                 print(type(i))
    #                 co += 1
                    
                cmd_len = len(processCmd)
                if cmd_len:
    #                 print('len=',cmd_len)
                    if 'Bootloader.py' in processCmd[cmd_len-1]:
                        print('############################\nFind Bootloader.py , ID:',processID)
                        info('Find Bootloader.py , ID:{}'.format(processID))
                        procIsRun = True
                
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    if procIsRun:
        print('Bootloader.py is Running')
    else:
        print('!!! Bootloader.py is Not Run')
        info('!!! Bootloader.py is Not Run')
        os.system('pwd')
        subprocess.run(['ls'], capture_output=True)
        try:
            print('Restart Bootloader')
            info('Restart Bootloader')
            subprocess.run(['python3','rasppi_firmware_4g/Bootloader.py'])
        except:
            print('Restart failt..')
            info('Restart failt..')
            pass
    time.sleep(30)