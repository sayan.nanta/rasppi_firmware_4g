## pi@raspberrypi:~ $ crontab -e
## @reboot sudo python3 rasppi_firmware_4g/Bootloader.py
## sudo pip3 install --upgrade requests

import subprocess
import os
import time
import time
import logging
import datetime
import _thread
# import Constants
# import MqttService as MqttService
# import SendData as SendData
import uuid
import os
import quantaAgrox as quantaAgrox
import xbee_pwr_ctl
import energy_mesure
import SendWDT
import bacup_data_service

try:
    # Open the file for writing.
    os.system('pwd')
    os.chdir("rasppi_firmware_4g")
    os.system('pwd')
except:
    pass


############ Generate New UUID ########################
try:
    import Constants
    import Quanta_Env
    UUID = Quanta_Env.UUID
    print('\n\nUUID:{}'.format(UUID))
    
except Exception as e:
    print('Err:{}'.format(e))
    import Constants
    Constants.UUID = New_uuid = str(uuid.uuid4())
    New_uuid = 'UUID="'+ New_uuid + '"\n'
    New_uuid = bytes(New_uuid,'utf-8')
    print('Generate new uuid')
    file = open('Quanta_Env.py', "w+b")
    file.write(New_uuid)
    file.close()
    print('Generate sucess:{}\nReboot system'.format(New_uuid))
    time.sleep(3)
    os.system('reboot')
    pass

### Init SysLed
import SysLed
SysLed.led()
SysLed.led.show(1,0x000000)
SysLed.led.show(2,0x000000)
SysLed.led.show(3,0x000000)
SysLed.led.show(4,0x000000)
time.sleep(10)

try:
    print('get cpuinfo')
    ls_cpuinfo = subprocess.run(['tail','/proc/cpuinfo'], stdout=subprocess.PIPE) # get last 10 line in file
    Constants.CPUINFO = ls_cpuinfo.stdout
    file = open('cpuinfo.txt', "w+b")
    file.write(ls_cpuinfo.stdout)
    file.close()
    print('*** Create file cpuinfo')
    ################ write cpu model to Constants
    ls_cpuinfo = subprocess.run(['tail','-1','/proc/cpuinfo'], stdout=subprocess.PIPE) # get last 10 line in file
    Constants.MODEL = ls_cpuinfo.stdout
except:
    pass

name = os.uname()
MachineNodename = name.nodename
if 'raspberrypi' or 'Raspberypi' in MachineNodename :
    MachineNodename = 'pi'
    print('\n\nMachine hardware is Raspberrypi')
else:
    MachineNodename = 'x86'
    print('\n\nMachine hardware is Computer')

if MachineNodename == 'pi':
    ls_file = subprocess.run(['ls','-al'], stdout=subprocess.PIPE)
    file = open('file_list.txt', "w+b")
    file.write(ls_file.stdout)
    file.close()
    
# try:
#     file = open('git_log.txt', "w+b")
#     if MachineNodename == 'pi':
#         print('Check update Firmware..')
#         retval = subprocess.run(['git','reset','--hard'], stdout=subprocess.PIPE)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','pull'], stdout=subprocess.PIPE)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','log','-n','1'], stdout=subprocess.PIPE)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         file.close()
# except Exception as err:
#     print('err:{}'.format(err))
#     pass
# time.sleep(10)

xbee_pwr_ctl.set_state(True)

import MqttService as MqttService
import SendData as SendData

def led_blink():
    time.sleep(0.15)
    SysLed.led.show(1,0x000000)
    time.sleep(0.15)
    SysLed.led.show(1,0x001F00)

class main:
    def __init__(self):
        buf = Constants.SOFTWARE_TITLE
        logging.info(buf)
        print(buf)

        ### Delay For Xbee boot ready
        led_blink()
        led_blink()
        led_blink()

        SysLed.led.show(1,0xFFFFFF)
        quantaAgrox.Agrox()
        time.sleep(1)
        
        SysLed.led.show(2,0xFFFFFF)
        _thread.start_new_thread(MqttService.MqttService, ())
        time.sleep(1)
        
        SysLed.led.show(3,0xFFFFFF) 
        _thread.start_new_thread(SendData.SendDataService, ())
        time.sleep(1)
        
        SysLed.led.show(4,0xFFFFFF)
        _thread.start_new_thread(quantaAgrox.Agrox.agrox_run, ())

        _thread.start_new_thread(SendWDT.Send_data_wathdog, ())

        _thread.start_new_thread(bacup_data_service.BackupData, ())

        time.sleep(1)
        energy_mesure.mesure()
        print('-Run energy_mesure')  
        SysLed.led.show(1,0x001F00)
        SysLed.led.show(2,0x000000)
        SysLed.led.show(3,0x000000)      
        SysLed.led.show(4,0x000200)
        _thread.start_new_thread(energy_mesure.mesure.read(), ())



        # print('Exit and Reboot')
        time.sleep(10)
        # os.system('reboot')
        while True:
            time.sleep(10)
        print('Exit Bootloader main')
        
        
if __name__ == '__main__':
    main()